package com.netmi.mykotlinlearning

import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.netmi.mykotlinlearning.fragment.FragmentA
import com.netmi.mykotlinlearning.fragment.FragmentB


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmenta = FragmentA()
        val fragmentb = FragmentB()


        val btFrag = findViewById<Button>(R.id.bt_frag);
        btFrag.tag = FragmentA.TAG
//        btFrag.setTag(fragmentb.tag)
        btFrag.setOnClickListener {
            showFragment(btFrag)
//            val  enter = R.anim.slide_in
//            val  exit = R.anim.fade_out
//            supportFragmentManager.beginTransaction().apply {
//                setCustomAnimations(enter, exit)
//                add(R.id.frag_a,fragmenta,fragmenta.tag) // this fragment gets the first animations
//                commit()
//            }


        }

    }


    private fun showFragment(curView: Button) {
        val enter = R.anim.slide_in
        val exit = R.anim.fade_out

        val fragmentManager = supportFragmentManager
        val fragmentTransaction =
            fragmentManager.beginTransaction()
        val tag = curView.tag as String
//        fragmentTransaction.setCustomAnimations(enter, exit)
        var fragment = fragmentManager.findFragmentByTag(tag)
        if (fragment == null) {
            fragment = Fragment.instantiate(this, tag)
            fragmentTransaction.add(R.id.frag_a, fragment, tag)
            fragmentTransaction.commitAllowingStateLoss()
        } else {
            val isHide = fragment.isHidden
            if (isHide) {
                fragmentTransaction.show(fragment)
                fragmentTransaction.commitAllowingStateLoss()
                fragment.onResume() //切换Fragment，实时刷新数据
            } else {
                hideFragment(curView)
            }
        }
    }

    private fun hideFragment(lastView: Button?) {
        if (lastView != null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            val tag = lastView.tag as String
            if (fragmentManager.findFragmentByTag(tag) != null) {
                val isHide = fragmentManager.findFragmentByTag(tag)!!.isHidden
                if (!isHide) {
                    fragmentTransaction.hide(fragmentManager.findFragmentByTag(tag)!!)
                    fragmentTransaction.commitAllowingStateLoss()
                }
            }
        }
    }

//    fun generateAnswerString(count: String): String {
//        val answerString = if (count.contains("tgw")) {
//            "I have the answer."
//        } else {
//            "The answer eludes me"
//        }
//
//        return answerString
//    }
//
//    /**您还可以将 return 关键字替换为赋值运算符：*/
//    fun generateAnswerStringTow(count: String): String = if (count.contains("tgw")) {
//        "I have the answer."
//    } else {
//        "The answer eludes me"
//    }
//
//    val stringLengthFunc: (String) -> Int = { input ->
//        input.length
//    }
//
//    val stringLength = stringLengthFunc("Android")
//
//
//    fun stringMapper(str: String, mapper: (String) -> Int): Int {
//        // Invoke function
//        return mapper(str)
//    }
//    fun sum(a: Int, b: Int) = a + b
//
//    fun sum1(a: Int, b: Int): Int {
//        return a + b
//    }
//
//    // 测试
//    fun main(args: Array<String>) {
//        val sumLambda: (Int, Int) -> Int = {x,y -> x+y}
//        println(sumLambda(1,2))  // 输出 3
//    }
//    // 测试
//    fun main1(args: Array<String>) {
//        val sumLambda: (x:Int, y:Int) -> Int = {x,y -> x+y}
//        println(sumLambda(1,2))  // 输出 3
//    }
//
//    private fun parseInt(s: String): String {
//            return s;
//    }
//    private fun parseString(s: String): String = s;
//
//
//    //使用is  做过类型判断以后，obj会被系统自动转换为String类型   这个问号代表是否可以返回 null --- is 相当于instanceof关键字
//    fun getStringLength(obj: Any): Int? {
//        if (obj is String) {
//            // 做过类型判断以后，obj会被系统自动转换为String类型
//            return obj.length
//        }
//        return null;
//    }
//
//    // !is 符号  在这个分支中, `obj` 的类型会被自动转换为 `String`  所以if语句中只能为 null  因为 返回类型要求int
//    fun getStringLengthNull(obj: Any): Int? {
//        if (obj !is String) {
//            // 做过类型判断以后，obj会被系统自动转换为String类型
//            return null
//        }
//        return obj.length;
//    }
//
//
//
//    fun printFor(x:Int,y:Int){
//        for (i in x..y) print(i) // 输出“1234”
//        //相当于  1 <= i && i <= 10
//
//        var person = Person("tgw")
//        person.lastName = "tgw1"
//    }
//
//    class MyClass {
//        companion object {
//            val myClassField1: Int = 1
//            var myClassField2 = "this is myClassField2"
//            fun companionFun1() {
//                println("this is 1st companion function.")
//                foo()
//            }
//
//            private fun foo() {
//                TODO("Not yet implemented")
//            }
//
//            fun companionFun2() {
//                println("this is 2st companion function.")
//                companionFun1()
//            }
//
//        }
//        fun MyClass.Companion.foo() {
//            println("伴随对象的扩展函数（内部）")
//        }
//        fun test2() {
//            MyClass.foo()
//        }
//        init {
//            test2()
//        }
//    }
//
//    val MyClass.Companion.no: Int
//        get() = 10
//    fun MyClass.Companion.foo() {
//        println("foo 伴随对象外部扩展函数")
//    }
//    fun main() {
//        println("no:${MyClass.no}")
//        println("field1:${MyClass.myClassField1}")
//        println("field2:${MyClass.myClassField2}")
//        MyClass.foo()
//        MyClass.companionFun2()
//        println("看看内部的")
//        var mc =MyClass()
//        //    mc.test2()
//    }

}
