package com.netmi.mykotlinlearning

/**
 * 类描述：基础语法操作
 * 创建人：tgw
 * 创建时间：2020/8/4
 * 修改备注：
 */
public open class Person constructor(  lastNam :String){

    //再次构造函数 需要: this(a)  a 参数代表 将值传递给lastNam
    constructor(a:String,b:String) : this(a) {

    }

    var lastNam2: String =lastNam

        get()=field
        set(value) {
            if (lastNam2 == "tgw"){

            }else{

            }
        }
//    lateinit var subject: TestSubject
//    @SetUp fun setup() {
//        subject = TestSubject()
//    }
//
//    @Test fun test() {
//        subject.method()  // dereference directly
//    }

    //加open 代表可覆盖
  open   var lastName: String = "zhang"
        get() = field.toUpperCase()   // 将变量赋值后转换为大写
        set

    open   var no: Int = 100
        get() = field                // 后端变量
        set(value) {
            if (value < 10) {       // 如果传入的值小于 10 返回该值
                field = value
            } else {
                field = -1         // 如果传入的值大于等于 10 返回 -1
            }
        }

    var heiht: Float = 145.4f
        private set
}

//注意父类有参数  子类Student必须为 Student() 形式而不是Student
//class Student:Person {....}   上句话原因，因为可以写成这样
class Student:Person,A,B{

    override var lastName : String ="tgw重写子类要加override  分类该参数要加open";
    /**次级构造函数**/
    constructor(name:String,age:Int):super(name){
        println("-------继承类次级构造函数---------")
        println("学生名： ${name}")
        println("年龄： ${age}")
        println("学生号： ${no}")
    }

    /**次级构造函数**/
    constructor(name:String,age:Int,no:String,score:Int):super(name,no){
        println("-------继承类次级构造函数---------")
        println("学生名： ${name}")
        println("年龄： ${age}")
        println("学生号： ${no}")
        println("成绩： ${score}")
    }

    override fun foo() {
        super<A>.foo()
        super<B>.foo()
    }

    override fun bar() {
        TODO("Not yet implemented")
    }

    // 扩展函数 swap,调换不同位置的值
    fun MutableList<Int>.swap(index1: Int, index2: Int) {
        val tmp = this[index1]     //  this 对应该列表
        this[index1] = this[index2]
        this[index2] = tmp
    }

    fun main(args: Array<String>) {

        val l = mutableListOf(1, 2, 3)
        // 位置 0 和 2 的值做了互换
        l.swap(0, 2) // 'swap()' 函数内的 'this' 将指向 'l' 的值
        var list1 = listOf("Java", "Kotlin", null, "Go")
        println(list1[0])
        for (i in list1.indices) {
            println(list1[i])
        }
        list1.forEach { it ->  println(it) }
        list1.forEach ({println(it)})



        //获取指定元素的出现位置
        println(list1.indexOf("Kotlin"))
        //获取List的子集合
        println(list1.subList(1, 3))
        println(l.toString())

        //fold——在给定初始值的基础上，从第一项到最后一项进行累加  比如这里我给的初始值为10,结果为16
        println(l.fold(10) { from, to -> from + to })
    }
}
interface A {
    fun foo() { print("A") }   // 已实现
    fun bar()                  // 未实现，没有方法体，是抽象的
}
interface B {
    fun foo()   { print("b") }    // 已实现
    fun bar()  { print("b") }                // 未实现，没有方法体，是抽象的
}