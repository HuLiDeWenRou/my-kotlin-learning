package com.netmi.mykotlinlearning.fragment

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.netmi.mykotlinlearning.R
import com.netmi.mykotlinlearning.utils.IntentUtils

/**
 * 类描述：
 * 创建人：tgw
 * 创建时间：2020/8/18
 * 修改备注：
 */
class FragmentA : Fragment() {
    companion object {
        public val TAG = FragmentA::class.java.name
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //这里设置动画 只要fragment出现或者消失都会调用
        val inflater = TransitionInflater.from(requireContext())
//        enterTransition = inflater.inflateTransition(R.transition.slide_right);
//        exitTransition = inflater.inflateTransition(R.transition.fade)

        sharedElementEnterTransition = TransitionInflater.from(requireContext())
            .inflateTransition(R.transition.shared_image)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragmenta_main, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val itemImageView = view.findViewById<ImageView>(R.id.item_image)
        ViewCompat.setTransitionName(itemImageView, "item_image")

        val btMap = view.findViewById<Button>(R.id.bt_map)
        btMap.setOnClickListener(View.OnClickListener {
            activity?.let { it1 -> IntentUtils.startMyMap(it1) }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }
}