package com.netmi.mykotlinlearning.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity
import com.netmi.mykotlinlearning.R

/**
 * 类描述：
 * 创建人：tgw
 * 创建时间：2020/8/18
 * 修改备注：
 */
class IntentUtils {

    companion object {

        //开启地图软件
        fun startMyMap(activity: Activity) {
            //以下是一个完整示例，展示了如何创建 Intent 来查看地图，验证是否存在可以处理该 Intent 的应用，然后启动它：
            val location: Uri =
                Uri.parse("geo:0,0?q=1600+Amphitheatre+Parkway,+Mountain+View,+California")
            // Or map point based on latitude/longitude
            // Uri location = Uri.parse("geo:37.422219,-122.08364?z=14"); // z param is zoom level
            // Or map point based on latitude/longitude
            // Uri location = Uri.parse("geo:37.422219,-122.08364?z=14"); // z param is zoom level
            val mapIntent = Intent(Intent.ACTION_VIEW, location)

            // Map point based on address
            val mapIntent1: Intent = Uri.parse(
                "geo:0,0?q=1600+Amphitheatre+Parkway,+Mountain+View,+California"
            ).let { location: Uri? ->
                // Or map point based on latitude/longitude
                // Uri location = Uri.parse("geo:37.422219,-122.08364?z=14"); // z param is zoom level
                Intent(Intent.ACTION_VIEW, location)
            }
            // Verify it resolves  验证是否存在
            val activities: List<ResolveInfo> =
                activity.packageManager.queryIntentActivities(mapIntent, 0)
            val isIntentSafe: Boolean = activities.isNotEmpty()

            // Always use string resources for UI text.
            // https://developer.android.google.cn/training/basics/intents/sending#kotlin
            // This says something like "Share this photo with"
            val title ="如果有多个地图应用，则每次都标记出可选项"
            // Create intent to show chooser
            val chooser = Intent.createChooser(mapIntent, title)

            // Start an activity if it's safe
            if (isIntentSafe && mapIntent.resolveActivity(activity.packageManager) != null) {
                activity.startActivity(mapIntent)
            }
        }

    }

}