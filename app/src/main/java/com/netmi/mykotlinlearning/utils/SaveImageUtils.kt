package com.netmi.mykotlinlearning.utils

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Environment
import android.provider.MediaStore

/**
 * 类描述：类描述：用于android 10.0及其以上 保存图片到本地 kotlin版
 * 创建人：tgw
 * 创建时间：2020/8/7
 * 修改备注：
 */
fun saveBitmap2Gallery2(context: Context, bitmap: Bitmap): Boolean {
    val name = System.currentTimeMillis().toString()
    val photoPath = Environment.DIRECTORY_DCIM + "/Camera"
    val contentValues = ContentValues().apply {
        put(MediaStore.MediaColumns.DISPLAY_NAME, name)
        put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            put(MediaStore.MediaColumns.RELATIVE_PATH, photoPath)//保存路径
            put(MediaStore.MediaColumns.IS_PENDING, true)
        }
    }
    val insert = context.contentResolver.insert(
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        contentValues
    ) ?: return false //为空的话 直接失败返回了


    //这个打开了输出流  直接保存图片就好了,做了null判断   实现了Closeable接口的对象可调用use函数
    var out = context.contentResolver.openOutputStream(insert)
    //这里的  it  相当于  out
    context.contentResolver.openOutputStream(insert).use {
        it ?: return false
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, it)
    }


//   context.contentResolver.openOutputStream(insert).use {
//       bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
//    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        contentValues.put(MediaStore.MediaColumns.IS_PENDING, false)
    }

    return true
}
